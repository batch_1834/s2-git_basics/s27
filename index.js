/*
	1. In the S27 folder, create an a1 folder and an index.js file inside of it.
	2. Create a simple server and the following routes with their corresponding HTTP methods and responses:
	a. If the url is http://localhost:4000/, send a response Welcome to Booking System
	b. If the url is http://localhost:4000/profile, send a response Welcome to your profile!
	c. If the url is http://localhost:4000/courses, send a response Here’s our courses available
	d. If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
	e. If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
	f. If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
	3. Test all the endpoints in Postman.

*/


let http = require("http");
let port = 4000;

http.createServer(function(request, response){
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"})
		response.end("Welcome to your profile!");
	}
	if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"})
		response.end("Here’s our courses available");
	}
	if(request.url == "/updatecourse" && request.method == "PUT"){
			let requestBody = ""

			request.on("data", (data) =>{
				requestBody += data;
			})

			request.on("end", () =>{
				requestBody = JSON.parse(requestBody);
				console.log(directory[1])
			})

		}
	else{
		response.writeHead(200, {"Content-Type":"text/plain"})
		response.end("Welcome to Booking System");
	}



}).listen(port);
console.log(`Server running at localhost: ${port}`);